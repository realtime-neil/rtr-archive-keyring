#!/bin/sh
# shellcheck disable=SC2317
# rtr-archive-keyring/jetring-create

set -eu

export LC_ALL=C

this="$(realpath "$0")"
readonly this="${this}"
here="$(dirname "${this}")"
readonly here="${here}"
whatami="$(basename "${this}")"
readonly whatami="${whatami}"
tmpdir="$(
    mktemp -dt "${whatami}.XXXXXX" \
        | xargs -I{} sh -c "chmod 1777 {} && echo {}"
)"
readonly tmpdir="${tmpdir}"
export TMPDIR="${tmpdir}"

cleanup() {
    status="$?"
    rm -rf "${tmpdir}" || true
    return "${status}"
}
trap cleanup EXIT

log() { echo "${whatami}[$$]:" "$@" >&2; }
error() { log "ERROR:" "$@"; }
warning() { log "WARNING:" "$@"; }
info() { log "INFO:" "$@"; }
die() {
    error "$@"
    usage >&2
    exit 1
}

usage() {
    cat <<EOF

Usage: $0 JETRING_DIR KEYID...
Create jetring directory from the given key id(s).

Options:

    -h    print usage and exit
    -k    keep downloaded pubkeys (default: delete after use)

Examples:

    \$ $0 -h

    \$ $0 rtr-archive-keyring.gpg.d 0xae0f78ce556b80ab48ce556f208dac2d8a68278a

EOF
}

################################################################################
################################################################################
################################################################################

while getopts ":hk" opt; do
    case "${opt}" in
        h)
            usage
            exit 0
            ;;
        k) requested_keep="true" ;;
        :) die "missing argument: -${OPTARG}" ;;
        \?) die "bad option: -${OPTARG}" ;;
    esac
done
shift "$((OPTIND - 1))"

if ! [ "$#" -ge 2 ]; then
    die "bad args"
fi
readonly jetring_dir="$1"
if ! mkdir -p "${jetring_dir}"; then
    die FAILURE: mkdir -p "${jetring_dir}"
fi
info jetring_dir: "${jetring_dir}"

shift

# https://manpages.debian.org/bullseye/gpg/gpg.1.en.html#keyring
#
# > --keyring file
#
# > Add file to the current list of keyrings. If file begins with a tilde and a
# > slash, these are replaced by the $HOME directory. If the filename does not
# > contain a slash, it is assumed to be in the GnuPG home directory
# > ("~/.gnupg" if --homedir or $GNUPGHOME is not used).
#
# > Note that this adds a keyring to the current list. If the intent is to use
# > the specified keyring alone, use --keyring along with --no-default-keyring.
#
# > If the option --no-keyring has been used no keyrings will be used at all.
#
# Therefore, this needs to be a full (canonical) path. Otherwise, 'gpg import'
# will import into ~/.gnupg/keyring.gpg, creating it if necessary.
keyring="$(mktemp -ut keyring.XXXXXX)"
readonly keyring="${keyring}"
info keyring: "${keyring}"

for keyid in "$@"; do
    if ! response="$(
        curl -fsSL "https://keys.openpgp.org/search?q=${keyid}"
    )"; then
        die FAILURE: curl -fsSL "https://keys.openpgp.org/search?q=${keyid}"
    fi
    info found keyid: "${keyid}"
    if ! pubkeyid="$(
        echo "${response}" \
            | grep -Ewo '[[:xdigit:]]{40}' \
            | tr '[:lower:]' '[:upper:]' \
            | sort -u \
            | xargs \
            | grep -Ex '[[:xdigit:]]{40}'
    )"; then
        die failed to parse pubkeyid
    fi
    info pubkeyid: "${pubkeyid}"
    if [ -f "${jetring_dir}"/add-"${pubkeyid}" ]; then
        warning extant file: "${jetring_dir}"/add-"${pubkeyid}"
        warning skipping pubkeyid: "${pubkeyid}"
        continue
    fi

    # I prefer openpgp over ubuntu because openpgp enforces email
    # confirmation. Sometimes a key owner has not (yet) confirmed their
    # identity. That causes this to happen:
    #
    #     $ gpg --keyserver keys.openpgp.org --recv-keys BC528686B50D79E339D3721CEB3E94ADBE1229CF
    #     gpg: keybox '/tmp/keyring.yQf7g0' created
    #     gpg: key EB3E94ADBE1229CF: new key but contains no user ID - skipped
    #     gpg: Total number processed: 1
    #     gpg:           w/o user IDs: 1
    #
    # Workaround: Use keyserver.ubuntu.com for now.
    if false; then
        pubkeyurl="https://keys.openpgp.org/vks/v1/by-fingerprint/${pubkeyid}"
    else
        pubkeyurl="https://keyserver.ubuntu.com/pks/lookup?op=get&search=0x${pubkeyid}"
    fi
    pubkeyout="${pubkeyid}".asc
    info downloading "${pubkeyurl}" to "${pubkeyout}"
    curl \
        --fail \
        --silent \
        --show-error \
        --location \
        --output "${pubkeyout}" \
        "${pubkeyurl}"

    info importing "${pubkeyout}" to "${keyring}"
    gpg \
        --import \
        --keyring "${keyring}" \
        --no-auto-check-trustdb \
        --no-default-keyring \
        --no-options \
        "${pubkeyout}"

    if ! [ "true" = "${requested_keep:-false}" ]; then
        info "$(rm -v "${pubkeyout}")"
    fi
done

info importing "${keyring}" to "${jetring_dir}"
jetring-explode "${keyring}" "${jetring_dir}"
info "$(rm -v "${keyring}")"

exit "$?"
