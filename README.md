rtr-archive-keyring
===================

This is a source package that builds several "keyring" and "apt config"
packages that support various third-party package repositories. For details
about what this means, please refer to
https://wiki.debian.org/DebianRepository/UseThirdParty .

# Keyring Packages

A binary package named `$FOO-archive-keyring` installs a GnuPG keyring file

```
/usr/share/keyrings/$FOO-archive-keyring.gpg
```

This keyring contains the public key(s) used by `apt` to verify the contents of
`$FOO` package repositories. These signatures are made by the `$FOO` repository
maintainer(s) using the associated private keys(s).

# Apt Config Packages

A binary package named `$FOO-archive-apt-config` installs a configuration

```
/etc/apt/sources.list.d/$FOO.sources
```

This config file allows `apt` to locate and use `$FOO` package repositories. A
`$FOO-archive-apt-config` package will often (but not always) require the
`$FOO-archive-keyring` package to supply the public key(s) used to verify the
contents of `$FOO` package repositories.

# Supported Repositories

## Intel OpenCL

### `intel-opencl-archive-keyring`
```
/usr/share/keyrings/intel-opencl-archive-keyring.gpg
```
```
pub   rsa4096 2018-07-30 [SC]
      E75D0B33338407411D357F51B9732172C4830B8F
uid                      Launchpad PPA for Intel OpenCL Neo team

```

### `intel-opencl-archive-apt-config`

* http://ppa.launchpad.net/intel-opencl/intel-opencl/ubuntu

## NodeSource Node.js Binary Distributions

### `node-archive-keyring`
```
/usr/share/keyrings/node-archive-keyring.gpg
```
```
pub   rsa4096 2014-06-13 [SC]
      9FD3B784BC1C6FC31A8A0A1C1655A0AB68576280
uid                      NodeSource <gpg@nodesource.com>
sub   rsa4096 2014-06-13 [E]

```

### `node-archive-apt-config`

* http://deb.nodesource.com/node_14.x
* http://deb.nodesource.com/node_16.x
* http://deb.nodesource.com/node_18.x

## Intel RealSense

### `realsense-archive-keyring`
```
/usr/share/keyrings/realsense-archive-keyring.gpg
```
```
pub   rsa2048 2018-01-09 [SC] [expires: 2028-01-08]
      F6E65AC044F831AC80A06380C8B3A55A6F3EFCDE
uid                      "CN = Intel(R) Intel(R) Realsense", O=Intel Corporation

```

### `realsense-archive-apt-config`

* http://librealsense.intel.com/Debian/apt-repo

## ROS

### `ros-archive-keyring`
```
/usr/share/keyrings/ros-archive-keyring.gpg
```
```
pub   rsa4096 2019-05-30 [SC] [expires: 2025-06-01]
      C1CF6E31E6BADE8868B172B4F42ED6FBAB17C654
uid                      Open Robotics <info@osrfoundation.org>

```

### `ros-archive-apt-config`

* http://packages.ros.org/ros2/ubuntu

## Realtime Robotics

### `rtr-archive-keyring`
```
/usr/share/keyrings/rtr-archive-keyring.gpg
```
```
pub   rsa4096 2020-03-16 [SC]
      AE0F78CE556B80AB48CE556F208DAC2D8A68278A
uid                      Launchpad PPA for realtime-robotics

pub   rsa4096 2020-08-26 [SCEA]
      423D522AA23B50C66F214F97219EACCE6579994C
uid                      https://packagecloud.io/realtime-robotics/rtr-robot-models (https://packagecloud.io/docs#gpg_signing) <support@packagecloud.io>
sub   rsa4096 2020-08-26 [SEA]

```

### `rtr-archive-apt-config`

* http://ppa.launchpad.net/realtime-robotics/arenasdk/ubuntu
* http://ppa.launchpad.net/realtime-robotics/cadexsdk/ubuntu
* http://ppa.launchpad.net/realtime-robotics/freecad/ubuntu
* http://ppa.launchpad.net/realtime-robotics/keyrings/ubuntu
* http://ppa.launchpad.net/realtime-robotics/krnx/ubuntu
* http://ppa.launchpad.net/realtime-robotics/misc/ubuntu
* http://ppa.launchpad.net/realtime-robotics/old/ubuntu
* http://ppa.launchpad.net/realtime-robotics/oldold/ubuntu
* http://ppa.launchpad.net/realtime-robotics/orocos-kdl/ubuntu
* http://ppa.launchpad.net/realtime-robotics/phoxi/ubuntu
* http://ppa.launchpad.net/realtime-robotics/ros/ubuntu
* http://ppa.launchpad.net/realtime-robotics/rtlinux/ubuntu
* http://ppa.launchpad.net/realtime-robotics/third-party/ubuntu
* http://ppa.launchpad.net/realtime-robotics/trac-ik/ubuntu
* http://ppa.launchpad.net/realtime-robotics/arenasdk/ubuntu
* http://ppa.launchpad.net/realtime-robotics/cadexsdk/ubuntu
* http://ppa.launchpad.net/realtime-robotics/freecad/ubuntu
* http://ppa.launchpad.net/realtime-robotics/keyrings/ubuntu
* http://ppa.launchpad.net/realtime-robotics/krnx/ubuntu
* http://ppa.launchpad.net/realtime-robotics/misc/ubuntu
* http://ppa.launchpad.net/realtime-robotics/old/ubuntu
* http://ppa.launchpad.net/realtime-robotics/oldold/ubuntu
* http://ppa.launchpad.net/realtime-robotics/orocos-kdl/ubuntu
* http://ppa.launchpad.net/realtime-robotics/phoxi/ubuntu
* http://ppa.launchpad.net/realtime-robotics/ros/ubuntu
* http://ppa.launchpad.net/realtime-robotics/rtlinux/ubuntu
* http://ppa.launchpad.net/realtime-robotics/third-party/ubuntu
* http://ppa.launchpad.net/realtime-robotics/trac-ik/ubuntu

## Ubuntu

### `ubuntu-archive-apt-config`

* http://archive.ubuntu.com/ubuntu
* http://security.ubuntu.com/ubuntu
