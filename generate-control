#!/bin/sh
# shellcheck disable=SC2317
# rtr-archive-keyring/generate-control

# This script generates the 'debian/control' file based on the things in the
# project (top) directory. I wrote this script because maintaining so many
# 'Package' stanzas "by hand" became error-prone and laborious.

set -eu

export LC_ALL=C

this="$(realpath "$0")"
readonly this="${this}"
here="$(dirname "${this}")"
readonly here="${here}"
whatami="$(basename "${this}")"
readonly whatami="${whatami}"

log() { echo "${whatami}[$$]: $*" >&2; }
error() { log "ERROR: $*"; }
warning() { log "WARNING: $*"; }
info() { log "INFO: $*"; }

longdescify() {
    tr -s '[:space:]' ' ' | fold -sw79 | sed -E 's/^/ /;s/[[:space:]]*$//' | awk 1
}

archive_apt_config_slugs() (
    find "${here}" -maxdepth 1 -type f \
        \( \
        \( \
        -name '*.sources.envsubst' \
        -exec basename -a -s '.sources.envsubst' {} + \
        \) \
        -o \
        \( \
        -name '*.sources.gen.sh' \
        -exec basename -a -s '.sources.gen.sh' {} + \
        \) \
        \) \
        | sort
)

archive_keyring_slugs() (
    find "${here}" -maxdepth 1 -type d -name '*-archive-keyring.gpg.d' \
        -exec basename -a -s '-archive-keyring.gpg.d' {} + | sort
)

################################################################################
################################################################################
################################################################################

# Always write to debian/control.
exec >"${here}"/debian/control

################################################################################

# Emit the "header".
cat <<'EOF'
Source: rtr-archive-keyring
Priority: important
Section: misc
Maintainer: Neil Roza <neil@rtr.ai>
Build-Depends:
 config-package-dev (>= 5.0),
 debhelper-compat (= 13),
 gettext-base,
 gnupg,
 jetring,
 shellcheck <!nocheck>,
 shfmt <!nocheck>,
Standards-Version: 4.6.2
Homepage: https://gitlab.com/realtime-robotics/rtr-archive-keyring
Vcs-Browser: https://gitlab.com/realtime-robotics/rtr-archive-keyring
Vcs-Git: https://gitlab.com/realtime-robotics/rtr-archive-keyring.git
EOF

################################################################################

# For each jetring directory of the form
#
#     ${slug}-archive-keyring.gpg.d
#
for slug in $(archive_keyring_slugs); do
    # ...emit the stanza for 'Package: ${slug}-archive-keyring'.

    # shellcheck disable=SC2030,SC2031
    export SLUG="${slug}"
    # shellcheck disable=SC2016
    envsubst '${SLUG}' <<'EOF'

Package: ${SLUG}-archive-keyring
Architecture: all
Multi-Arch: allowed
Depends: ${misc:Depends}
Description: GnuPG public keys for ${SLUG} repositories
EOF
    # shellcheck disable=SC2016
    envsubst '${SLUG}' <<'EOF' | longdescify
This package contains the public keys used to verify the signed indices within
the ${SLUG} package repositories.
EOF
done

################################################################################

# For each apt sources.list envsubst template file of the form
#
#     ${slug}.sources.envsubst
#
for slug in $(archive_apt_config_slugs); do
    # ...emit the stanza for 'Package: ${slug}-archive-apt-config'.

    # shellcheck disable=SC2030,SC2031
    export SLUG="${slug}"
    if [ 'ubuntu' = "${slug}" ]; then
        # special case: ubuntu-keyring provides the keyrings (and trusted keys)
        # required by our ubuntu-archive-apt-config
        export DEPENDS_KEYRING="ubuntu-keyring"
    else
        export DEPENDS_KEYRING="${slug}-archive-keyring (= \${binary:Version})"
    fi
    # shellcheck disable=SC2016
    envsubst '${SLUG},${DEPENDS_KEYRING}' <<'EOF'

Package: ${SLUG}-archive-apt-config
Architecture: all
Multi-Arch: allowed
Depends: ${misc:Depends}, ${DEPENDS_KEYRING}
Provides: ${diverted-files}
Conflicts: ${diverted-files}
Description: apt config files for ${SLUG} repositories
EOF
    # shellcheck disable=SC2016
    envsubst '${SLUG}' <<'EOF' | longdescify
This package contains the apt configuration files used to locate and use the
${SLUG} package repositories.
EOF
done

################################################################################

# Emit the stanza for 'Package: rtr-archive-all'.
cat <<'EOF'

Package: rtr-archive-all
Section: metapackages
Architecture: all
Multi-Arch: allowed
Depends:
 ${misc:Depends},
EOF
{
    for slug in $(archive_keyring_slugs); do
        # shellcheck disable=SC2016
        printf ' %s-archive-keyring (= ${binary:Version}),\n' "${slug}"
    done
    for slug in $(archive_apt_config_slugs); do
        # shellcheck disable=SC2016
        printf ' %s-archive-apt-config (= ${binary:Version}),\n' "${slug}"
    done
} | sort
cat <<'EOF'
Description: metapackage for keyrings and apt configs
EOF
# shellcheck disable=SC2016
cat <<'EOF' | longdescify
This package depends on all the archive keyrings and archive apt configs
provided by the ${S:Source} source package.
EOF

wrap-and-sort -abst

exit "$?"
