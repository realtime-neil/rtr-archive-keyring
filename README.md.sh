#!/bin/sh

cat <<'EOF'
rtr-archive-keyring
===================

This is a source package that builds several "keyring" and "apt config"
packages that support various third-party package repositories. For details
about what this means, please refer to
https://wiki.debian.org/DebianRepository/UseThirdParty .

# Keyring Packages

A binary package named `$FOO-archive-keyring` installs a GnuPG keyring file

```
/usr/share/keyrings/$FOO-archive-keyring.gpg
```

This keyring contains the public key(s) used by `apt` to verify the contents of
`$FOO` package repositories. These signatures are made by the `$FOO` repository
maintainer(s) using the associated private keys(s).

# Apt Config Packages

A binary package named `$FOO-archive-apt-config` installs a configuration

```
/etc/apt/sources.list.d/$FOO.sources
```

This config file allows `apt` to locate and use `$FOO` package repositories. A
`$FOO-archive-apt-config` package will often (but not always) require the
`$FOO-archive-keyring` package to supply the public key(s) used to verify the
contents of `$FOO` package repositories.

# Supported Repositories
EOF

find \
    "$(dirname "$(realpath "$0")")" \
    -maxdepth 1 \
    -type f \
    -name '*.sources' \
    | sort \
    | while read -r sources; do
        if ! [ -s "${sources}" ]; then
            continue
        fi
        # My convention:
        #
        # line 1: path comment
        #
        # line 3: descriptive name
        #
        # line 5: website url

        printf '\n## %s\n' "$(sed -n '3s/^# //p' "${sources}")"
        archive_keyring_gpg="${sources%.sources}"-archive-keyring.gpg
        if [ -f "${archive_keyring_gpg}" ]; then
            # shellcheck disable=SC2016
            printf '\n### `%s`\n' "$(basename "${archive_keyring_gpg}" .gpg)"
            echo '```'
            printf "/usr/share/keyrings/%s\n" "$(basename "${archive_keyring_gpg}")"
            echo '```'
            echo '```'
            (
                GNUPGHOME="$(mktemp -dt GNUPGHOME.XXXXXX)"
                export GNUPGHOME
                gpg \
                    --import \
                    --import-options=show-only \
                    --no-auto-check-trustdb \
                    --no-default-keyring \
                    --no-keyring \
                    --no-options \
                    "${archive_keyring_gpg}"
                rm -rf "${GNUPGHOME}"
            )
            echo '```'
        fi
        # shellcheck disable=SC2016
        printf '\n### `%s-archive-apt-config`\n' "$(basename "${sources}" .sources)"
        echo
        grep-dctrl \
            --ensure-dctrl \
            --no-field-names \
            --show-field=URIs \
            --pattern="" \
            "${sources}" \
            | xargs -n1 printf '* %s\n'

    done

exit "$?"
