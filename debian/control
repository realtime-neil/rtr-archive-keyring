Source: rtr-archive-keyring
Priority: important
Section: misc
Maintainer: Neil Roza <neil@rtr.ai>
Build-Depends:
 config-package-dev (>= 5.0),
 debhelper-compat (= 13),
 gettext-base,
 gnupg,
 jetring,
 shellcheck <!nocheck>,
 shfmt <!nocheck>,
Standards-Version: 4.6.2
Homepage: https://gitlab.com/realtime-robotics/rtr-archive-keyring
Vcs-Browser: https://gitlab.com/realtime-robotics/rtr-archive-keyring
Vcs-Git: https://gitlab.com/realtime-robotics/rtr-archive-keyring.git

Package: intel-opencl-archive-apt-config
Architecture: all
Multi-Arch: allowed
Depends:
 intel-opencl-archive-keyring (= ${binary:Version}),
 ${misc:Depends},
Provides:
 ${diverted-files},
Conflicts:
 ${diverted-files},
Description: apt config files for intel-opencl repositories
 This package contains the apt configuration files used to locate and use the
 intel-opencl package repositories.

Package: intel-opencl-archive-keyring
Architecture: all
Multi-Arch: allowed
Depends:
 ${misc:Depends},
Description: GnuPG public keys for intel-opencl repositories
 This package contains the public keys used to verify the signed indices within
 the intel-opencl package repositories.

Package: node-archive-apt-config
Architecture: all
Multi-Arch: allowed
Depends:
 node-archive-keyring (= ${binary:Version}),
 ${misc:Depends},
Provides:
 ${diverted-files},
Conflicts:
 ${diverted-files},
Description: apt config files for node repositories
 This package contains the apt configuration files used to locate and use the
 node package repositories.

Package: node-archive-keyring
Architecture: all
Multi-Arch: allowed
Depends:
 ${misc:Depends},
Description: GnuPG public keys for node repositories
 This package contains the public keys used to verify the signed indices within
 the node package repositories.

Package: realsense-archive-apt-config
Architecture: all
Multi-Arch: allowed
Depends:
 realsense-archive-keyring (= ${binary:Version}),
 ${misc:Depends},
Provides:
 ${diverted-files},
Conflicts:
 ${diverted-files},
Description: apt config files for realsense repositories
 This package contains the apt configuration files used to locate and use the
 realsense package repositories.

Package: realsense-archive-keyring
Architecture: all
Multi-Arch: allowed
Depends:
 ${misc:Depends},
Description: GnuPG public keys for realsense repositories
 This package contains the public keys used to verify the signed indices within
 the realsense package repositories.

Package: ros-archive-apt-config
Architecture: all
Multi-Arch: allowed
Depends:
 ros-archive-keyring (= ${binary:Version}),
 ${misc:Depends},
Provides:
 ${diverted-files},
Conflicts:
 ${diverted-files},
Description: apt config files for ros repositories
 This package contains the apt configuration files used to locate and use the
 ros package repositories.

Package: ros-archive-keyring
Architecture: all
Multi-Arch: allowed
Depends:
 ${misc:Depends},
Description: GnuPG public keys for ros repositories
 This package contains the public keys used to verify the signed indices within
 the ros package repositories.

Package: rtr-archive-all
Section: metapackages
Architecture: all
Multi-Arch: allowed
Depends:
 intel-opencl-archive-apt-config (= ${binary:Version}),
 intel-opencl-archive-keyring (= ${binary:Version}),
 node-archive-apt-config (= ${binary:Version}),
 node-archive-keyring (= ${binary:Version}),
 realsense-archive-apt-config (= ${binary:Version}),
 realsense-archive-keyring (= ${binary:Version}),
 ros-archive-apt-config (= ${binary:Version}),
 ros-archive-keyring (= ${binary:Version}),
 rtr-archive-apt-config (= ${binary:Version}),
 rtr-archive-keyring (= ${binary:Version}),
 ubuntu-archive-apt-config (= ${binary:Version}),
 ${misc:Depends},
Description: metapackage for keyrings and apt configs
 This package depends on all the archive keyrings and archive apt configs
 provided by the ${S:Source} source package.

Package: rtr-archive-apt-config
Architecture: all
Multi-Arch: allowed
Depends:
 rtr-archive-keyring (= ${binary:Version}),
 ${misc:Depends},
Provides:
 ${diverted-files},
Conflicts:
 ${diverted-files},
Description: apt config files for rtr repositories
 This package contains the apt configuration files used to locate and use the
 rtr package repositories.

Package: rtr-archive-keyring
Architecture: all
Multi-Arch: allowed
Depends:
 ${misc:Depends},
Description: GnuPG public keys for rtr repositories
 This package contains the public keys used to verify the signed indices within
 the rtr package repositories.

Package: ubuntu-archive-apt-config
Architecture: all
Multi-Arch: allowed
Depends:
 ubuntu-keyring,
 ${misc:Depends},
Provides:
 ${diverted-files},
Conflicts:
 ${diverted-files},
Description: apt config files for ubuntu repositories
 This package contains the apt configuration files used to locate and use the
 ubuntu package repositories.
